# Otus.Teaching.PromoCodeFactory

Проект для домашних заданий и демо по курсу `C# ASP.NET Core Разработчик` от `Отус`.
Cистема `Promocode Factory` для выдачи промокодов партнеров для клиентов по группам предпочтений.

Данный проект является стартовой точкой для Homework №3

Подробное описание проекта можно найти в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Home)

Описание домашнего задания в [Homework Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-3)

#Как настроить долбанные раннеры в GitLab

1. На странице Settings|CI/CD проекта GitLab найти раздел Runners, Нажать кнопку Expand
2. Нажать кнопку New Project Runner
3. На открывшейся странице New Project runner не забыть снять галку Run untagged jobs
4. Запустить в командной строке с админскими правами .\gitlab-runner.exe register . Тулза будет запрашивать параметры. Использовать токен, отображающийся на открывшейся странице "Runner created", на вопрос Enter an executor: kubernetes, docker-windows, docker-ssh, parallels, shell, docker+machine, instance, custom, docker, ssh, virtualbox, docker-ssh+machine выбрать shell
5. На странице Settings|CI/CD в разделе Runners Отключить переключатель Enable shared runners for this project
6. Изменить в c:\GitLab-Runner\config.toml настройку shell на следующую shell = "powershell"
